const defaultConfigOptions = {
    'watch_on_alt': true,
    'channel_videos_tab': true,
    'youtube_without_polymer': false,

    'youtube_alt': 'cadencegq',

    'hooktube_search_on_youtube': false,
    'hooktube_channel_on_youtube': false,

    'invidious_autoplay_video': false,
    'invidious_search_on_youtube': false,
    'invidious_channel_on_youtube': false,

    'cadencegq_search_on_youtube': false,
    'cadencegq_channel_on_youtube': false,
};

// just to keep a list
const supported_alts_code = [
    'hooktube',
    'invidious',
    'cadencegq',
];