function replaced_hostname(url, newHostname) {
    let curl = new URL(url);
    curl.hostname = newHostname;
    return curl.toString();
}

function replace_with_youtube(url) {
    let curl = new URL(url);
    curl.hostname = 'www.youtube.com';
    return curl.toString();
}

function cadencegq_video_page(url) {
    const curl = new URL(url);
    const videoHash = curl.searchParams.get('v');
    return `https://cadence.gq/cloudtube/video/${videoHash}`;
}

function invidious_search_to_youtube(url) {
    let curl = new URL(url);
    curl.hostname = 'www.youtube.com';
    curl.pathname = '/results';
    curl.searchParams.append('search_query', curl.searchParams.get('q'));
    curl.searchParams.delete('q');
    return curl.toString();
}
