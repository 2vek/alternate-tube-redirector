async function listenerOnTabChange(option, changeHandler, filterUrls, filterProps, rev) {
    const defaultOption= {[option]: defaultConfigOptions[option]};
    const savedOptions = await browser.storage.local.get(defaultOption);

    if (savedOptions[option]) {
        if (rev)
            browser.tabs.onUpdated.removeListener(changeHandler);
        else
            browser.tabs.onUpdated.addListener(
                changeHandler,
                { urls: filterUrls, properties: filterProps}
            );
    } else {
        if (rev)
            browser.tabs.onUpdated.addListener(
                changeHandler,
                { urls: filterUrls, properties: filterProps}
            );
        else
            browser.tabs.onUpdated.removeListener(changeHandler);
    }
}

function watchOnStorageChange_tab(option, changeHandler, filterUrls, filterProps, rev) {
    browser.storage.onChanged.addListener((changes, area) => {
        if (area === 'local' && option in changes) {
            listenerOnTabChange(option, changeHandler, filterUrls, filterProps, rev);
        }
    });
}

// needed because https://bugzilla.mozilla.org/show_bug.cgi?id=1443221
function registerTabListener(option, changeHandler, filterUrls, filterProps, rev) {
    listenerOnTabChange(option, changeHandler, filterUrls, filterProps, rev);
    watchOnStorageChange_tab(option, changeHandler, filterUrls, filterProps, rev);
}