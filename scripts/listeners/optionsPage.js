async function loadConfigOption() {
    const savedOptions = await browser.storage.local.get(defaultConfigOptions);
    Object.entries(savedOptions).forEach(([option, val]) => {
        const Opt = document.getElementById(option);

        if(typeof val === 'boolean') {
            Opt.checked = val;
        } else {
            Opt.value = val;
        }

        Opt.addEventListener('change', async () => {
            const currentOption = {
                [option]: (typeof val === 'boolean') ? Opt.checked : Opt.value
            };
            await browser.storage.local.set(currentOption);
            if(option === 'watch_on_alt') {
                updateButtonInfo();
            }
        });
    });
}

document.addEventListener('DOMContentLoaded', loadConfigOption);