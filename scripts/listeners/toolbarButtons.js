browser.runtime.onInstalled.addListener(updateButtonInfo);
browser.runtime.onStartup.addListener(updateButtonInfo);

async function toggleButtonState() {
    const option = 'watch_on_alt';
    const defaultOption = {[option]: true};
    const savedOptions = await browser.storage.local.get(defaultOption);
    const updatedOptions = { [option]: !savedOptions[option] };
    await browser.storage.local.set(updatedOptions);
    await updateButtonInfo();
}

browser.browserAction.onClicked.addListener(toggleButtonState);
