registerRequestsListener(
    'cadencegq_channel_on_youtube',
    async (request) => {
        let curl = new URL(request.url);
        const chanHash = curl.pathname.split('/')[3];
        const newUrl = `https://www.youtube.com/channel/${chanHash}/`;
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://cadence.gq/cloudtube/channel/*'
    ],
    ['main_frame']
);