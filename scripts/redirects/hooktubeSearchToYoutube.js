registerRequestsListener(
    'hooktube_search_on_youtube',
    async (request) => {
        const newUrl = replace_with_youtube(request.url);
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://hooktube.com/results*'
    ],
    ['main_frame']
);