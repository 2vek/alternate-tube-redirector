registerRequestsListener(
    'hooktube_channel_on_youtube',
    async (request) => {
        const newUrl = replace_with_youtube(request.url);
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://hooktube.com/channel/*'
    ],
    ['main_frame']
);