registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        const url = new URL(request.url);
        const hash = url.pathname.split('/')[1];
        const newUrl = `https://www.youtube.com/watch?v=${hash}`;
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://youtu.be/*'
    ],
    ['main_frame']
);