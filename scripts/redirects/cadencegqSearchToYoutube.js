registerRequestsListener(
    'cadencegq_search_on_youtube',
    async (request) => {
        const curl = new URL(request.url);
        const searchTerms = curl.searchParams.get('q');
        const newUrl = `https://www.youtube.com/results?search_query=${searchTerms}`;
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://cadence.gq/cloudtube/search*'
    ],
    ['main_frame']
);