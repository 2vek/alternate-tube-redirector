registerRequestsListener(
    'invidious_autoplay_video',
    async (request) => {
        let url = new URL(request.url);
        if(!url.searchParams.has('autoplay')) {
            if(request.type === 'main_frame') {
                let params = new URLSearchParams(url.search);
                params.set('autoplay', 1);
                url.search = params.toString();
                const newUrl = url.toString();
                return {
                    redirectUrl: newUrl
                };
            } else {
                await browser.tabs.reload(request.tabId);
            }
        }
    },
    [
        '*://invidio.us/watch*',
    ],
    ['main_frame']
);