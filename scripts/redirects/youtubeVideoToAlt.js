function from_alt_video_page(fromUrl) {
    if (fromUrl) {
        const url = new URL(fromUrl);
        const hostnames = [
            'hooktube.com',
            'invidio.us',
            'cadence.gq'
        ];
        if (hostnames.indexOf(url.hostname)) return true;
    }
    return false;
}

async function alt_video_page(vurl) {
    let newUrl = '';
    const url = new URL(vurl);
    if (url.searchParams.has('list') && !url.searchParams.has('index')) {
        url.pathname = '/playlist';
        const list = url.searchParams.get('list');
        url.search = '';
        url.searchParams.set('list', list);
        newUrl = url.toString();
    } else {
        const option = 'youtube_alt';
        const defaultOption = {[option]: defaultConfigOptions[option]};
        const savedOption = await browser.storage.local.get(defaultOption);    

        switch (savedOption[option]) {
        case 'hooktube':
            newUrl = replaced_hostname(url, 'hooktube.com');
            break;
        case 'invidious':
            newUrl = replaced_hostname(url, 'invidio.us');
            break;
        case 'cadencegq':
            newUrl = cadencegq_video_page(url);
        }
    }
    return newUrl;
}

registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        let url = new URL(request.url);
        if ((url.pathname === '/watch') && !from_alt_video_page(request.originUrl)) {
            if (request.type === 'main_frame') {
                const newUrl = await alt_video_page(request.url);
                return {
                    redirectUrl: newUrl
                };
            } else {
                await browser.tabs.reload(request.tabId);
            }
        }
    },
    [
        '*://www.youtube.com/watch*',
    ],
    ['main_frame', 'xmlhttprequest']
);

registerTabListener(
    'watch_on_alt',
    async (tabId, changeInfo, updatedTab) => {
        const currentTab = await browser.tabs.get(tabId);
        // TODO: make watch on youtube button work
        if (from_alt_video_page(currentTab.url) && changeInfo.url) {
            const newUrl = await alt_video_page(changeInfo.url);
            browser.tabs.update(tabId, { url: newUrl });
        }
    },
    [
        '*://www.youtube.com/watch*',
        '*://hooktube.com/watch*',
        '*://invidio.us/watch*',
        '*://cadence.gq/cloudtube/video/*'
    ],
    ['status']
);