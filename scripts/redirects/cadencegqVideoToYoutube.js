registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        const curl = new URL(url);
        const videoHash = curl.pathname.split('/')[3];
        const newUrl = `https://www.youtube.com/watch?v=${videoHash}`;
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://cadence.gq/cloudtube/video/*'
    ],
    ['main_frame'],
    true
);