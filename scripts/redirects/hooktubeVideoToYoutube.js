registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        const newUrl = replace_with_youtube(request.url);
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://hooktube.com/watch*'
    ],
    ['main_frame'],
    true
);